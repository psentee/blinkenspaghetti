use anyhow::Result;
use pastel::named::NamedColor;
use quote::quote;

pub fn generate() -> Result<String> {
    let colors: Vec<_> = pastel::named::NAMED_COLORS
        .iter()
        .map(|NamedColor { name, color }| {
            let rgba = color.to_rgba();
            let palette::Srgb {
                red, green, blue, ..
            } = palette::Srgb::new(rgba.r, rgba.g, rgba.b).into_format::<f32>();
            let uname = syn::parse_str::<syn::Ident>(&name.to_uppercase()).unwrap();
            quote! {
                pub const #uname: Srgb = Srgb {
                    red: #red,
                    green: #green,
                    blue: #blue,
                    standard,
                };
            }
        })
        .collect();

    let module = quote! {
        use palette::Srgb;

        #[allow(non_upper_case_globals)]
        const standard: core::marker::PhantomData<palette::encoding::srgb::Srgb> = core::marker::PhantomData;

        #(#colors)*
    };

    Ok(prettyplease::unparse(&syn::parse2(module)?))
}

{
  description = "Dev environment";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    # If we need to pin rust-nightly again:
    # rust-manifest = {
    #   url = "https://static.rust-lang.org/dist/2023-07-08/channel-rust-nightly.toml";
    #   flake = false;
    # };
  };
  outputs = {
    self,
    nixpkgs,
    fenix,
    flake-utils,
  }:
    flake-utils.lib.eachSystem
    (with flake-utils.lib.system; [x86_64-linux aarch64-linux]) (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [fenix.overlays.default];
      };
      inherit (pkgs) lib;
    in {
      devShells.default = let
        selectFenix = fenix: fenix.latest; # fenix.fromManifestFile rust-manifest;
        rust = pkgs.fenix.combine [
          # rust nightly needed for RTIC 2.0
          (selectFenix pkgs.fenix).toolchain
          (selectFenix pkgs.fenix.targets.thumbv7em-none-eabihf).rust-std
        ];

        ## CHOOSE WISELY:
        firmwareLinker = "${pkgs.llvmPackages_latest.lld}/bin/ld.lld"; # <- needs to be unwrapped, Nix magic clashes with embedded toolchain magic
        # firmwareLinker = "${pkgs.gcc-arm-embedded}/bin/arm-none-eabi-ld";

        runtimeDependencies = [
          pkgs.libglvnd
          pkgs.wayland
        ];
      in
        pkgs.mkShell {
          packages =
            [
              rust
              # pkgs.cargo-binutils
              pkgs.cargo-expand
              pkgs.cargo-outdated
              pkgs.llvmPackages.bintools
              pkgs.llvmPackages.lldb
              pkgs.teensy-loader-cli
              # pkgs.gcc-arm-embedded
              # for new player:
              pkgs.pkg-config
              pkgs.libxkbcommon
              # debugging
              pkgs.picocom
              pkgs.radare2
            ]
            ++ runtimeDependencies;
          LD_LIBRARY_PATH = lib.makeLibraryPath runtimeDependencies;
          RUST_BACKTRACE = 1;

          # Teensy LC
          CARGO_TARGET_THUMBV6M_NONE_EABI_LINKER = firmwareLinker;

          # Teensy 4.0
          CARGO_TARGET_THUMBV7EM_NONE_EABIHF_LINKER = firmwareLinker;
        };
    });
}

use embedded_hal::blocking::serial::Write;

type Lpuart = bsp::board::Lpuart6;

pub(crate) static mut EMERGENCY_UART: Option<Lpuart> = None;

/// Safety: writes to static mut, must be in critical section
pub(crate) unsafe fn init(lpuart: Lpuart) {
    let _ = EMERGENCY_UART.insert(lpuart);
    debug_log("\r\n\r\nINITIALIZED LOGGING\r\n\n");
}

/// safety: actually safe(ish), but let's force callers to jump through a hoop
pub(crate) unsafe fn debug_log(msg: &str) {
    cortex_m::interrupt::free(|_| {
        if let Some(lpuart) = EMERGENCY_UART.as_mut() {
            let _ = lpuart.bwrite_all(msg.as_bytes());
        }
    });
}

pub(crate) unsafe fn debug_value<T: core::fmt::Debug>(value: T) -> T {
    use core::fmt::Write;
    let mut buf: heapless::String<255> = heapless::String::new();
    writeln!(&mut buf, "{:?}", &value).unwrap();
    debug_log(&buf);
    value
}

#[panic_handler]
pub fn panic(info: &core::panic::PanicInfo) -> ! {
    cortex_m::interrupt::disable();

    if let Some(mut lpuart) = unsafe {
        // Safety: the worst has already happened
        EMERGENCY_UART.take()
    } {
        let _ = lpuart.bwrite_all("\r\n\nPANIK PANIK PANIK\r\n\n".as_bytes());

        if let Some(msg) = info.payload().downcast_ref::<&str>() {
            let _ = lpuart.bwrite_all(msg.as_bytes());
        } else {
            let mut buf: heapless::String<255> = heapless::String::new();

            if {
                use core::fmt::Write;
                write!(&mut buf, "{}\r\n", info)
            }
            .is_err()
            {
                let _ = lpuart.bwrite_all("\r\nFailed to write! to string\r\n".as_bytes());
            } else {
                let _ = lpuart.bwrite_all(buf.as_bytes());
            }
        }
    }

    // // info.payload().downcast_ref::<&str>().unwrap()

    // // red LED
    // unsafe { t4::gpio::Pin22::<t4::gpio::Output>::fiat() }
    //     .set_high()
    //     .ok();

    // let mut console =
    //     console::Console::new(unsafe { core::mem::transmute::<(), t4::dev::LPUART6>(()) });

    // #[cfg(debug_assertions)]
    // {
    //     let mut msg: String<U256> = String::new();
    //     display_to_string(&mut msg, &_info);
    //     log!(console, "PANIC: {}", msg.as_str());
    // }

    // #[cfg(not(debug_assertions))]
    // {
    //     // in release mode, just try payload as string (FIXME: can we
    //     // do better?) + location
    //     // let payload = info
    //     //     .payload()
    //     //     .downcast_ref::<&str>()
    //     //     .unwrap_or(&"UNKNOWN PAYLOAD TYPE");

    //     // let (file, line, column) = match info.location() {
    //     //     None => ("[no location]", 0, 0),
    //     //     Some(location) =>  (location.file(), location.line(), location.column()),
    //     // };

    //     // log!(
    //     //     console,
    //     //     "PANIC at {}:{}:{}: {}",
    //     //     file,
    //     //     line,
    //     //     column,
    //     //     payload
    //     // );

    //     log!(console, "PANIC PANIC PANIC");
    // }

    teensy4_panic::sos()
}

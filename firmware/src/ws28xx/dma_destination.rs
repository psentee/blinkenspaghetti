use bsp::{hal, ral};

pub(super) struct Ws28xxDestination<'a, const N: u8> {
    flexio: &'a ral::flexio::Instance<N>,
    destination_signal: u32,
    shifter_id: usize,
}

impl<'a, const N: u8> Ws28xxDestination<'a, N> {
    pub(super) fn new(
        flexio: &'a ral::flexio::Instance<N>,
        destination_signal: u32,
        shifter_id: usize,
    ) -> Self {
        Self {
            flexio,
            destination_signal,
            shifter_id,
        }
    }
}

unsafe impl<const N: u8> hal::dma::peripheral::Destination<u32> for Ws28xxDestination<'_, N> {
    fn destination_signal(&self) -> u32 {
        self.destination_signal
    }

    fn destination_address(&self) -> *const u32 {
        &self.flexio.SHIFTBUFBBS[self.shifter_id] as *const _ as _
    }

    fn enable_destination(&mut self) {
        let mask: u32 = 1 << self.shifter_id;
        ral::modify_reg!(ral::flexio, self.flexio, SHIFTSDEN, |v| v | mask);
    }

    fn disable_destination(&mut self) {
        let mask: u32 = 1 << self.shifter_id;
        ral::modify_reg!(ral::flexio, self.flexio, SHIFTSDEN, |v| v & !mask);
    }
}

use std::{env, path::Path};

const INPUT_FILE: &str = "config.ron";
const CHAIN_INPUT_FILE: &str = "chain.ron";
const OUTPUT_FILE: &str = "coords.rs";
const COLORS_FILE: &str = "colors.rs";

fn main() {
    let config_ron_path = Path::new(&env::var_os("CARGO_MANIFEST_DIR").unwrap()).join(
        if env::var_os("CARGO_FEATURE_CHAIN").is_none() {
            INPUT_FILE
        } else {
            CHAIN_INPUT_FILE
        },
    );
    let config = coordgen::Config::load(config_ron_path).unwrap();
    let coords_rs_path = Path::new(&env::var_os("OUT_DIR").unwrap()).join(OUTPUT_FILE);
    let colors_rs_path = Path::new(&env::var_os("OUT_DIR").unwrap()).join(COLORS_FILE);
    std::fs::write(coords_rs_path, coordgen::generate(&config).unwrap()).unwrap();
    std::fs::write(colors_rs_path, coordgen::colorgen::generate().unwrap()).unwrap();
    println!("cargo:rerun-if-changed=build.rs");
    println!("cargo:rerun-if-changed=coords.ron");
    println!("cargo:rerun-if-changed=chain.ron");
}

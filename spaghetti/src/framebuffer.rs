pub union FrameBuffer<const N: usize, const W: usize> {
    pixels: [[u8; 3]; N],
    words: [u32; W],
}

pub const fn framebuffer_words(leds: usize) -> usize {
    leds * 3 / 4 + 2
}

impl<const N: usize, const W: usize> FrameBuffer<N, W> {
    const CHECK: () = {
        if W != framebuffer_words(N) {
            panic!("Wrong W for this N");
        }
    };

    pub const fn new() -> Self {
        #[allow(clippy::let_unit_value)]
        let _ = Self::CHECK;
        Self { words: [0; W] }
    }

    pub fn load_pixels(&mut self, pixels: impl Iterator<Item = [u8; 3]>) {
        for (r, w) in pixels.zip(unsafe { self.pixels.iter_mut() }) {
            *w = r;
        }
    }

    pub fn words(&self) -> &[u32; W] {
        unsafe { &self.words }
    }

    pub fn pixels(&self) -> &[[u8; 3]; N] {
        unsafe { &self.pixels }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fb() {
        let mut fb = FrameBuffer::<3, { framebuffer_words(3) }>::new();

        fb.load_pixels([[255, 127, 63], [31, 15, 7], [3, 1, 0]].into_iter());
        assert_eq!(fb.words(), &[0x1f3f7fff, 0x103070f, 0, 0]);
    }
}

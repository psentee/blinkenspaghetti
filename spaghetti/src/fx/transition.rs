use super::_prelude::*;

pub async fn fade(rate: f32, sh: &mut impl Show) {
    loop {
        let mut finished: bool = true;
        for px in sh.frame_mut().iter_mut() {
            px.darken_assign(rate);
            finished &= px.red + px.green + px.blue < 0.001
        }
        sh.frame_done().await;
        if finished {
            break;
        }
    }
}

use core::ops::Range;

use heapless::Vec;

use super::_prelude::*;

const START_LENGTH: usize = 6;

#[derive(Debug, Copy, Clone)]
struct Food {
    at: usize,
    frames_left: u32,
}

impl Food {
    const N: usize = 5;
    const GOOD_FOR: Range<u32> = Duration::secs(5).ticks()..Duration::secs(23).ticks();

    const FAKE: Self = Self {
        at: usize::MAX,
        frames_left: 0,
    };

    fn new_at(at: usize, rng: &mut impl Rng) -> Self {
        Self {
            at,
            frames_left: rng.gen_range(Self::GOOD_FOR),
        }
    }

    fn is_real(&self) -> bool {
        self.at < Coords::N
    }

    fn is_gone(&self) -> bool {
        self.frames_left == 0
    }

    fn needs_placing(&self) -> bool {
        self.is_gone() || !self.is_real()
    }

    fn disappear(&mut self) {
        *self = Self::FAKE;
    }

    fn tick(&mut self) {
        self.frames_left = self.frames_left.saturating_sub(1);
    }

    fn color(&self) -> Srgb {
        const COLOR: Srgb = crate::colors::ANTIQUEWHITE;
        const FADE: u32 = Duration::secs(2).ticks();
        if self.frames_left > FADE {
            COLOR
        } else {
            COLOR.darken((FADE - self.frames_left) as f32 / FADE as f32)
        }
    }
}

pub struct Snek {
    pos: Vec<usize, { Coords::MAX_STRAND_LEN }>,
    stuck: bool,
    food: [Food; Food::N],
    left: usize,
    ctr: usize,
}

impl Snek {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        Self {
            pos: Vec::new(),
            stuck: false,
            food: [Food::FAKE; Food::N],
            left: usize::MAX,
            ctr: 0,
        }
    }

    const EMPTY: Srgb = Srgb::new(0.025, 0.025, 0.025);

    fn head(&self) -> usize {
        *self.pos.last().unwrap()
    }

    fn next(&mut self, rng: &mut impl Rng) -> Option<usize> {
        let all_neighbours = COORDS.neighbours[self.head()];
        let neighbours: heapless::Vec<usize, { Coords::MAX_NEIGHBOURS }> = all_neighbours
            .iter()
            .cloned()
            .filter(|n| !self.pos.contains(n))
            .collect();

        if neighbours.is_empty() {
            return None;
        }

        Some(*neighbours.choose(rng).unwrap())
    }

    fn hatch(&mut self, rng: &mut impl Rng) {
        'retry: loop {
            self.pos.clear();
            unsafe {
                // Safety: pos is empty
                self.pos.push_unchecked(rng.gen_range(0..Coords::N))
            };
            for _ in 0..(START_LENGTH - 1) {
                if let Some(next) = self.next(rng).or_else(|| {
                    self.pos.reverse();
                    self.next(rng)
                }) {
                    unsafe {
                        // Safety: pos is almost empty
                        self.pos.push_unchecked(next)
                    };
                } else {
                    continue 'retry;
                }
            }
            break;
        }
        self.place_food(rng);
        self.stuck = false;
        self.ctr = 0;
    }

    fn place_food(&mut self, rng: &mut impl Rng) {
        for i in 0..self.food.len() {
            if self.food[i].needs_placing() {
                loop {
                    let pos = rng.gen_range(0..Coords::N);
                    if !self.pos.contains(&pos)
                        && self.food.iter().position(|food| food.at == pos).is_none()
                    {
                        self.food[i] = Food::new_at(pos, rng);
                        break;
                    }
                }
            }
        }
    }

    /// true means we died
    fn calculate(&mut self, rng: &mut impl Rng) -> bool {
        self.ctr += 1;

        self.place_food(rng);

        let done = if self.pos.is_empty() {
            // First frame.
            self.hatch(rng);
            false
        } else if let Some(next) = self.next(rng) {
            self.pos.push(next).unwrap();
            self.stuck = false;

            if let Some(food_idx) = self.food.iter().position(|&fp| fp.at == next) {
                self.food[food_idx].disappear();
                self.pos.is_full()
            } else {
                self.left = self.pos.remove(0);
                false
            }
        } else if self.stuck {
            // we're stuck second time, give up
            true
        } else {
            // We reached a dead end, try to turn back
            self.pos.reverse();
            self.left = usize::MAX;
            self.stuck = true;
            false
        };

        self.food.iter_mut().for_each(|food| food.tick());

        done
    }

    fn render(&self, fr: &mut Frame) {
        if self.left < usize::MAX {
            fr[self.left] = Self::EMPTY;
        }

        for food in self.food.iter().filter(|food| food.is_real()) {
            fr[food.at] = if food.is_gone() {
                Self::EMPTY
            } else {
                food.color().darken(libm::sinf(
                    ((self.ctr >> 2) & 255) as f32 / 4. * core::f32::consts::TAU,
                ))
            };
        }

        self.pos.iter().rev().enumerate().for_each(|(i, &pos)| {
            let hue = i as f32 * 360. / self.pos.len() as f32;
            fr[pos] = palette::Hsv::new_srgb_const(hue.into(), 1.0, 1.0).into_color();
        });
    }
}

pub async fn snek(sh: &mut impl Show) {
    sh.frame_mut().fill(Snek::EMPTY);
    sh.frame_done().await;
    let mut snake = Snek::new();
    loop {
        let done = snake.calculate(sh.rng());
        snake.render(sh.frame_mut());
        sh.frame_done().await;
        if done {
            break;
        }
    }

    // Wait for half a second
    for _ in 0..15 {
        sh.frame_done().await;
    }

    super::transition::fade(0.05, sh).await;
}

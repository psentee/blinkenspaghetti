use super::_prelude::*;

const RADIUS: f32 = 36.0;
const RESISTANCE: f32 = 0.998;
const GRAVITY: Fector = Fector::new(0.0, 0.1);

pub async fn bounce(sh: &mut impl Show) {
    let area = Frect::new(
        Foint::new(RADIUS, RADIUS),
        euclid::Size2D::splat(255.0 - 2.0 * RADIUS),
    );
    let mut center = Foint::new(128.0, RADIUS);
    let mut v = Fector::from_angle_and_length(
        Angle::radians(sh.rng().gen()),
        sh.rng().gen_range(3.2..10.0),
    );
    let mut hue_start: f32 = 0.0;

    loop {
        for (px, pt) in sh.frame_mut().iter_mut().zip(COORDS.coords.iter()) {
            let ft = Foint::new(pt.x.into(), pt.y.into());
            let dist = ft.distance_to(center);
            if dist > RADIUS {
                // crate::colors::BLACK
                px.darken_assign(0.33);
            } else {
                let value: f32 = if dist < RADIUS * 0.8 {
                    1.0
                } else {
                    1.0 - (RADIUS - dist) / (RADIUS * 0.2)
                };
                *px = Hsv::new_srgb_const(
                    ((1.0 - dist / RADIUS) * 360. + hue_start).into(),
                    value,
                    value,
                )
                .into_color();
            };
        }

        let next = center + v;

        if !area.x_range().contains(&next.x) {
            v = Fector::new(-v.x, v.y);
            center = center + v;
        } else if !area.y_range().contains(&next.y) {
            v = Fector::new(v.x, -v.y);
            center = center + v;
        } else {
            center = next;
        }

        v = (v + GRAVITY) * RESISTANCE;
        hue_start += 2.3;

        if sh.frame_done().await.should_finish() || v.length() < 0.1 {
            break;
        }
    }
}

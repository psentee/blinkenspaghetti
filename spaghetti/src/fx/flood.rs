use super::_prelude::*;

pub const SIZE: f32 = 96.;
pub const FADE: f32 = 24.;
pub const DIAMETER: f32 = 362.;

pub async fn flood(angle: f32, sh: &mut impl Show) {
    let direction = Fector::from_angle_and_length(Angle::radians(angle), 1.0);
    let origin = Foint::new(128., 128.) - direction * DIAMETER / 2.;

    // TODO: find actual distance to start/finish?
    for frame_num in 0..((DIAMETER + SIZE) as usize) {
        let mut visible = false;
        for i in 0..Coords::N {
            let pos = frame_num as f32
                - (origin - COORDS.coords[i].to_f32())
                    .project_onto_vector(direction)
                    .length();

            if pos > 0. {
                sh.frame_mut()[i] = colors::ALICEBLUE;
            } else {
                sh.frame_mut()[i] = colors::MEDIUMVIOLETRED;
            }

            if pos < 0. || pos >= SIZE {
                sh.frame_mut()[i] = Default::default();
                continue;
            }
            visible = true;

            let v: f32 = if pos < FADE {
                pos / FADE
            } else if pos < SIZE - FADE {
                1.0
            } else {
                (SIZE - pos) / FADE
            };

            sh.frame_mut()[i] =
                Hsv::new_srgb((frame_num as f32 * 2.3) + pos * 360. / SIZE, 1.0, v).into_color();
        }

        if visible {
            sh.frame_done().await;
        }
    }
    sh.frame_done().await;
}

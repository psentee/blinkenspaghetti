pub(crate) use eframe::egui;
pub(crate) use egui::{CentralPanel, Color32, Key, Modifiers, Pos2, Rect, Stroke, Vec2};

pub(crate) trait ResponseExt {
    fn clicked_or_shortcut(&self, key: &egui::KeyboardShortcut) -> bool;
}

impl ResponseExt for egui::Response {
    fn clicked_or_shortcut(&self, key: &egui::KeyboardShortcut) -> bool {
        self.clicked() || self.ctx.input_mut(|i| i.consume_shortcut(key))
    }
}

pub(crate) trait KeyExt {
    fn as_shortcut(&self) -> egui::KeyboardShortcut;
}

impl KeyExt for Key {
    fn as_shortcut(&self) -> egui::KeyboardShortcut {
        egui::KeyboardShortcut::new(Modifiers::NONE, *self)
    }
}

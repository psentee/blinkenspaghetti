use std::f32::consts::TAU;

use eframe::egui::Sense;
use eframe::egui::SidePanel;
use eframe::emath::RectTransform;
use eframe::epaint::Shape;
use spaghetti::{Point2D, COORDS};

use crate::player::{Playable, Player};

mod prelude;

use prelude::*;

pub(crate) struct App {
    player: Player,
    step: usize,

    // UI state
    show_strands: bool,
    pble: Playable,
}

impl App {
    pub(crate) fn new(player: Player) -> Self {
        Self {
            player,
            step: 30,
            show_strands: false,
            pble: Playable::Main,
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        if ctx.input_mut(|i| {
            i.consume_shortcut(&Key::Escape.as_shortcut())
                || i.consume_shortcut(&Key::Q.as_shortcut())
        }) {
            frame.close();
            return;
        }

        ctx.input_mut(|i| {
            if i.consume_shortcut(&Key::Space.as_shortcut()) {
                if self.player.stopped() {
                    self.player.play(self.pble);
                } else {
                    self.player.toggle_pause();
                }
            }
        });

        SidePanel::left("left").show(ctx, |ui| {
            ui.heading("Effect");
            for pble in Playable::all() {
                ui.radio_value(&mut self.pble, pble, pble.to_string());
            }

            ui.horizontal(|ui| {
                ui.group(|ui| {
                    use egui::widgets::Button;
                    const SPACE: f32 = 4.;

                    let stopped = self.player.stopped();

                    if ui.selectable_label(self.player.paused(), "⏸").clicked() {
                        self.player.toggle_pause();
                    }

                    ui.add_space(SPACE);
                    if ui
                        .selectable_label(!stopped, "▶")
                        .on_hover_text("Play")
                        .clicked()
                    {
                        self.player.play(self.pble);
                    }

                    ui.add_space(SPACE);
                    if ui
                        .selectable_label(stopped, "⏹")
                        .on_hover_text("Stop")
                        .clicked()
                    {
                        self.player.stop();
                    }

                    ui.add_space(SPACE);
                    ui.add(egui::DragValue::new(&mut self.step).fixed_decimals(0));
                    if ui
                        .add_enabled(!stopped, Button::new("⏩"))
                        .on_hover_text("FF")
                        .clicked()
                    {
                        self.player.skip(self.step);
                    }

                    ui.add_space(SPACE);
                    if ui.add_enabled(!stopped, Button::new("１⏩")).clicked() {
                        self.player.skip(1);
                    }
                });
            });

            ui.horizontal(|ui| {
                ui.add_sized(
                    Vec2::new(40.0, ui.available_height()),
                    egui::widgets::Label::new(format!("#{}", self.player.frame_number())),
                )
                .on_hover_text("Frame number");

                ui.add(
                    egui::Slider::from_get_set(0.1..=1000.0, |v| {
                        if let Some(fps) = v {
                            self.player.set_fps(fps);
                        }
                        self.player.fps()
                    })
                    .max_decimals(1)
                    .min_decimals(0)
                    .logarithmic(true)
                    .suffix(" FPS"),
                );
            });

            ui.separator();
            ui.heading("UI");
            egui::widgets::global_dark_light_mode_buttons(ui);
            ui.checkbox(&mut self.show_strands, "Show strands");
        });

        CentralPanel::default().show(ctx, |ui| {
            use egui::Align::Center;

            const LED_RADIUS: f32 = 8.0;

            let led_stroke = ui.visuals().widgets.inactive.fg_stroke;

            let square = Vec2::splat(ui.available_size().min_elem());
            let layout = if square.y == ui.available_height() {
                // full height, center horizontally
                egui::Layout::top_down(Center)
            } else {
                // full width, center vertically
                egui::Layout::right_to_left(Center)
            };
            ui.with_layout(layout, |ui| {
                let (resp, painter) = ui.allocate_painter(square, Sense::hover());
                let margin = LED_RADIUS + led_stroke.width + 0.23;
                painter.rect_filled(resp.rect, margin, ui.visuals().faint_bg_color);
                let rect = resp.rect.shrink(margin);
                let led2ui = RectTransform::from_to(
                    Rect::from_min_size(Pos2::ZERO, Vec2::splat(255.)),
                    rect,
                );

                let mut hovered_led: Option<usize> = None;

                for (i, px) in self.player.frame().iter().enumerate() {
                    let pos = led_pos(i, led2ui);

                    let resp = ui
                        .interact(
                            Rect::from_center_size(pos, Vec2::splat(LED_RADIUS * 2.0)),
                            ui.id().with(("led", i)),
                            Sense::hover(),
                        )
                        .context_menu(|ui| {
                            ui.heading(format!("#{}", i));
                            ui.label(format!("neighbours: {:?}", COORDS.neighbours[i],));
                        });

                    if resp.hovered() {
                        for neighbour in COORDS.neighbours[i].iter().map(|&i| led_pos(i, led2ui)) {
                            painter.line_segment([pos, neighbour], led_stroke);
                        }
                        hovered_led = Some(i);
                    }

                    let px: palette::Srgb<u8> = px.into_format();

                    painter.circle(
                        pos,
                        LED_RADIUS,
                        Color32::from_rgba_unmultiplied(px.red, px.green, px.blue, 128),
                        led_stroke,
                    )
                }

                if self.show_strands {
                    let any_strand_stroke = ui.visuals().widgets.inactive.fg_stroke;
                    let current_strand_stroke = ui.visuals().widgets.hovered.fg_stroke;
                    for strand in COORDS.strands.iter() {
                        let stroke = if hovered_led.is_some_and(|hovered| strand.contains(&hovered))
                        {
                            current_strand_stroke
                        } else {
                            any_strand_stroke
                        };
                        painter.add(Shape::line(
                            strand.clone().map(|i| led_pos(i, led2ui)).collect(),
                            stroke,
                        ));
                    }

                    painter.extend(
                        COORDS
                            .strands
                            .iter()
                            .zip(COORDS.strands.iter().skip(1))
                            .flat_map(|(s1, s2)| {
                                let (color, radius) = if hovered_led.is_some_and(|hovered| {
                                    s1.contains(&hovered) || s2.contains(&hovered)
                                }) {
                                    (current_strand_stroke.color, 0.67)
                                } else {
                                    (any_strand_stroke.color, 0.5)
                                };

                                let start = led_pos(s1.end - 1, led2ui);
                                let end = led_pos(s2.start, led2ui);
                                let mut shapes =
                                    Shape::dotted_line(&[start, end], color, 4.0, radius);

                                let angle = (start - end).normalized().angle();
                                let v1 = end + Vec2::angled(angle - TAU / 16.) * 2. * LED_RADIUS;
                                let v2 = end + Vec2::angled(angle + TAU / 16.) * 2. * LED_RADIUS;
                                shapes.push(Shape::convex_polygon(
                                    vec![end, v1, v2],
                                    color,
                                    Stroke::NONE,
                                ));

                                shapes
                            }),
                    );
                }
            });
        });
    }
}

fn led_pos(idx: usize, xform: RectTransform) -> Pos2 {
    let Point2D { x, y, .. } = COORDS.coords[idx];
    xform * Pos2::new(x.into(), y.into())
}

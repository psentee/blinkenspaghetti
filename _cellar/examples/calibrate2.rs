#![feature(never_type)]
#![no_std]
#![no_main]

extern crate embedded_hal;

use cichlid::ColorRGB as RGB;
use cortex_m_rt::{entry, pre_init};

use teensy_lc;
use teensy_lc::prelude::*;

use blinkenspaghettimonster::ws2812b::WS2812B;

// C5 is LED
// B1 is 17 / 5V out

#[pre_init]
unsafe fn init() {
    teensy_lc::init();
}

const N_LEDS: usize = 600;
const DIV: u8 = 16;

#[entry]
fn main() -> ! {
    let pp = teensy_lc::dev::Peripherals::take().unwrap();
    let cpp = teensy_lc::dev::CorePeripherals::take().unwrap();

    //// Turn LED on
    // Port C Clock Gate Control: enable clock to port B & C
    pp.SIM
        .scgc5
        .write(|w| w.portc().set_bit().portb().set_bit());

    let fgpioc = pp.FGPIOC.split();
    fgpioc.pc5.enable();
    let mut led_pin = fgpioc.pc5.into_output();
    led_pin.set_high().unwrap();

    let fgpiob = pp.FGPIOB.split();
    fgpiob.pb1.enable();
    // Enable pulldown on B1
    pp.PORTB.pcr1.modify(|_, w| w.pe().set_bit().ps()._0());
    let mut ws2812b_pin = fgpiob.pb1.into_output();
    ws2812b_pin.set_low().unwrap();
    teensy_lc::delay_usec(1_000_000);
    led_pin.set_low().unwrap();
    let mut syst = cpp.SYST;
    let mut ws2812b = WS2812B::new(&mut ws2812b_pin, &mut syst);

    let on = RGB::White / DIV;
    let mut frame: [RGB; N_LEDS] = [on; N_LEDS];

    // strip 0 beginning
    frame[0] = RGB::Yellow / DIV;
    frame[1] = RGB::Yellow / DIV;

    // strip 1 beginning
    frame[60] = RGB::Red / DIV;
    frame[61] = RGB::Yellow / DIV;

    // strip 2 beginning
    frame[120] = RGB::Green / DIV;
    frame[121] = RGB::Yellow / DIV;

    // strip 3 beginning
    frame[180] = RGB::Blue / DIV;
    frame[181] = RGB::Yellow / DIV;

    // strip 4 beginning
    frame[240] = RGB::Yellow / DIV;
    frame[241] = RGB::Red / DIV;

    // strip 5 beginning
    frame[300] = RGB::Red / DIV;
    frame[301] = RGB::Red / DIV;

    // strip 6 beginning
    frame[360] = RGB::Green / DIV;
    frame[361] = RGB::Red / DIV;

    // strip 7 beginning
    frame[420] = RGB::Blue / DIV;
    frame[421] = RGB::Red / DIV;

    // strip 8 beginning
    frame[480] = RGB::Yellow / DIV;
    frame[481] = RGB::Green / DIV;

    // strip 9 beginning
    frame[540] = RGB::Red / DIV;
    frame[541] = RGB::Green / DIV;

    ws2812b.send(&frame);
    loop {}
}

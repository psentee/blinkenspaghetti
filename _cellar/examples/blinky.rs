#![no_std]
#![no_main]

use cortex_m;
use cortex_m_rt::{entry, pre_init};

use blinkenspaghettimonster::teensy_lc::{self, dev};

#[pre_init]
unsafe fn init_teensy_lc() {
    teensy_lc::init();
}

const RELOAD_MAX: u32 = 0x00FFFFFF;

#[entry]
fn main() -> ! {
    let pp = dev::Peripherals::take().unwrap();
    let cpp = dev::CorePeripherals::take().unwrap();

    //// Initialize clock
    let mut syst = cpp.SYST;

    syst.set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
    syst.set_reload(48_000_000 / 4);
    syst.clear_current();
    syst.enable_counter();
    // syst.enable_interrupt();

    //// Initialize LED to on
    // Port C Clock Gate Control: enable clock to port C
    pp.SIM.scgc5.write(|w| w.portc().set_bit());

    // Port C pin control register #5, bits 10-8 001: set port C to GPIO
    pp.PORTC.pcr5.write(|w| w.mux()._001());

    // Fast GPIO port C / port data direction register; bit 5 out
    pp.FGPIOC.pddr.write(|w| unsafe { w.bits(1 << 5) });

    // psor -> port set output register, set bit 5, LED on
    pp.FGPIOC.psor.write(|w| unsafe { w.bits(1 << 5) });

    let mut i: u16 = 0;
    loop {
        if syst.has_wrapped() {
            i += 1;
            if i % 4 == 0 {
                pp.FGPIOC.ptor.write(|w| unsafe { w.bits(1 << 5) });
            }
            // i += 1;
            // if i > 500 {
            //    panic!("boo!");
            // }
        }
    }
}

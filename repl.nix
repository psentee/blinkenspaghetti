#/usr/bin/env nix repl
let
  _root = builtins.toString ./.;
  self = builtins.getFlake _root;
  pkgs = import self.inputs.nixpkgs {
    system = builtins.currentSystem;
    overlays = [self.inputs.fenix.overlays.default];
  };
in rec {
  inherit _root self pkgs;
  inherit (self) inputs sourceInfo;
  inherit (pkgs) lib system fenix;
}
